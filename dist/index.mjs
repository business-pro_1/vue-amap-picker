/**
 * name: vue-amap-picker
 * version: v2.1.0
 * description: vue3高德地图pc的地址拖拽搜索选点神器
 * author: liyingqi <834777203@qq.com>
 * homepage: https://gitee.com/liyingqi1234/vue-amap-picker
 */
import { defineComponent as V, openBlock as M, createElementBlock as L, createElementVNode as A, toRefs as T, reactive as Y, onMounted as R, onUnmounted as ee, nextTick as $, resolveComponent as C, normalizeStyle as oe, createVNode as F, Fragment as B, renderList as J, withModifiers as ne, createBlock as O, withCtx as z, createTextVNode as te, toDisplayString as P, createCommentVNode as E, normalizeClass as ie } from "vue";
import { ElIcon as ae, ElEmpty as se, ElInput as re, ElMessage as le } from "element-plus";
/*! Element Plus Icons Vue v2.3.1 */
var ce = /* @__PURE__ */ V({
  name: "Location",
  __name: "location",
  setup(t) {
    return (y, p) => (M(), L("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      viewBox: "0 0 1024 1024"
    }, [
      A("path", {
        fill: "currentColor",
        d: "M800 416a288 288 0 1 0-576 0c0 118.144 94.528 272.128 288 456.576C705.472 688.128 800 534.144 800 416M512 960C277.312 746.688 160 565.312 160 416a352 352 0 0 1 704 0c0 149.312-117.312 330.688-352 544"
      }),
      A("path", {
        fill: "currentColor",
        d: "M512 512a96 96 0 1 0 0-192 96 96 0 0 0 0 192m0 64a160 160 0 1 1 0-320 160 160 0 0 1 0 320"
      })
    ]));
  }
}), D = ce, ue = /* @__PURE__ */ V({
  name: "Search",
  __name: "search",
  setup(t) {
    return (y, p) => (M(), L("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      viewBox: "0 0 1024 1024"
    }, [
      A("path", {
        fill: "currentColor",
        d: "m795.904 750.72 124.992 124.928a32 32 0 0 1-45.248 45.248L750.656 795.904a416 416 0 1 1 45.248-45.248zM480 832a352 352 0 1 0 0-704 352 352 0 0 0 0 704"
      })
    ]));
  }
}), N = ue;
function W(t, y = 1e3, p = !1, f) {
  let I = null, i = !1;
  function e(...s) {
    if (console.log("防抖1123131"), I !== null && clearTimeout(I), !i && p) {
      const m = t.apply(this, s);
      typeof f == "function" && f(m), i = !0;
    }
    I = setTimeout(() => {
      t.apply(this, s), i = !1;
    }, y);
  }
  return e;
}
var de = typeof globalThis < "u" ? globalThis : typeof window < "u" ? window : typeof global < "u" ? global : typeof self < "u" ? self : {};
function pe(t) {
  return t && t.__esModule && Object.prototype.hasOwnProperty.call(t, "default") ? t.default : t;
}
var G = { exports: {} };
(function(t, y) {
  (function(p, f) {
    t.exports = f();
  })(de, function() {
    function p(o) {
      var l = [];
      return o.AMapUI && l.push(f(o.AMapUI)), o.Loca && l.push(I(o.Loca)), Promise.all(l);
    }
    function f(o) {
      return new Promise(function(l, a) {
        var c = [];
        if (o.plugins)
          for (var r = 0; r < o.plugins.length; r += 1)
            e.AMapUI.plugins.indexOf(o.plugins[r]) == -1 && c.push(o.plugins[r]);
        if (s.AMapUI === i.failed)
          a("前次请求 AMapUI 失败");
        else if (s.AMapUI === i.notload) {
          s.AMapUI = i.loading, e.AMapUI.version = o.version || e.AMapUI.version, r = e.AMapUI.version;
          var h = document.body || document.head, v = document.createElement("script");
          v.type = "text/javascript", v.src = "https://webapi.amap.com/ui/" + r + "/main.js", v.onerror = function(d) {
            s.AMapUI = i.failed, a("请求 AMapUI 失败");
          }, v.onload = function() {
            if (s.AMapUI = i.loaded, c.length)
              window.AMapUI.loadUI(c, function() {
                for (var d = 0, _ = c.length; d < _; d++) {
                  var U = c[d].split("/").slice(-1)[0];
                  window.AMapUI[U] = arguments[d];
                }
                for (l(); m.AMapUI.length; )
                  m.AMapUI.splice(0, 1)[0]();
              });
            else
              for (l(); m.AMapUI.length; )
                m.AMapUI.splice(0, 1)[0]();
          }, h.appendChild(v);
        } else
          s.AMapUI === i.loaded ? o.version && o.version !== e.AMapUI.version ? a("不允许多个版本 AMapUI 混用") : c.length ? window.AMapUI.loadUI(c, function() {
            for (var d = 0, _ = c.length; d < _; d++) {
              var U = c[d].split("/").slice(-1)[0];
              window.AMapUI[U] = arguments[d];
            }
            l();
          }) : l() : o.version && o.version !== e.AMapUI.version ? a("不允许多个版本 AMapUI 混用") : m.AMapUI.push(function(d) {
            d ? a(d) : c.length ? window.AMapUI.loadUI(c, function() {
              for (var _ = 0, U = c.length; _ < U; _++) {
                var x = c[_].split("/").slice(-1)[0];
                window.AMapUI[x] = arguments[_];
              }
              l();
            }) : l();
          });
      });
    }
    function I(o) {
      return new Promise(function(l, a) {
        if (s.Loca === i.failed)
          a("前次请求 Loca 失败");
        else if (s.Loca === i.notload) {
          s.Loca = i.loading, e.Loca.version = o.version || e.Loca.version;
          var c = e.Loca.version, r = e.AMap.version.startsWith("2"), h = c.startsWith("2");
          if (r && !h || !r && h)
            a("JSAPI 与 Loca 版本不对应！！");
          else {
            r = e.key, h = document.body || document.head;
            var v = document.createElement("script");
            v.type = "text/javascript", v.src = "https://webapi.amap.com/loca?v=" + c + "&key=" + r, v.onerror = function(d) {
              s.Loca = i.failed, a("请求 AMapUI 失败");
            }, v.onload = function() {
              for (s.Loca = i.loaded, l(); m.Loca.length; )
                m.Loca.splice(0, 1)[0]();
            }, h.appendChild(v);
          }
        } else
          s.Loca === i.loaded ? o.version && o.version !== e.Loca.version ? a("不允许多个版本 Loca 混用") : l() : o.version && o.version !== e.Loca.version ? a("不允许多个版本 Loca 混用") : m.Loca.push(function(d) {
            d ? a(d) : a();
          });
      });
    }
    if (!window)
      throw Error("AMap JSAPI can only be used in Browser.");
    var i;
    (function(o) {
      o.notload = "notload", o.loading = "loading", o.loaded = "loaded", o.failed = "failed";
    })(i || (i = {}));
    var e = { key: "", AMap: { version: "1.4.15", plugins: [] }, AMapUI: { version: "1.1", plugins: [] }, Loca: { version: "1.3.2" } }, s = { AMap: i.notload, AMapUI: i.notload, Loca: i.notload }, m = { AMap: [], AMapUI: [], Loca: [] }, b = [], S = function(o) {
      typeof o == "function" && (s.AMap === i.loaded ? o(window.AMap) : b.push(o));
    };
    return { load: function(o) {
      return new Promise(function(l, a) {
        if (s.AMap == i.failed)
          a("");
        else if (s.AMap == i.notload) {
          var c = o.key, r = o.version, h = o.plugins;
          c ? (window.AMap && location.host !== "lbs.amap.com" && a("禁止多种API加载方式混用"), e.key = c, e.AMap.version = r || e.AMap.version, e.AMap.plugins = h || e.AMap.plugins, s.AMap = i.loading, r = document.body || document.head, window.___onAPILoaded = function(d) {
            if (delete window.___onAPILoaded, d)
              s.AMap = i.failed, a(d);
            else
              for (s.AMap = i.loaded, p(o).then(function() {
                l(window.AMap);
              }).catch(a); b.length; )
                b.splice(0, 1)[0]();
          }, h = document.createElement("script"), h.type = "text/javascript", h.src = "https://webapi.amap.com/maps?callback=___onAPILoaded&v=" + e.AMap.version + "&key=" + c + "&plugin=" + e.AMap.plugins.join(","), h.onerror = function(d) {
            s.AMap = i.failed, a(d);
          }, r.appendChild(h)) : a("请填写key");
        } else if (s.AMap == i.loaded)
          if (o.key && o.key !== e.key)
            a("多个不一致的 key");
          else if (o.version && o.version !== e.AMap.version)
            a("不允许多个版本 JSAPI 混用");
          else {
            if (c = [], o.plugins)
              for (r = 0; r < o.plugins.length; r += 1)
                e.AMap.plugins.indexOf(o.plugins[r]) == -1 && c.push(o.plugins[r]);
            c.length ? window.AMap.plugin(c, function() {
              p(o).then(function() {
                l(window.AMap);
              }).catch(a);
            }) : p(o).then(function() {
              l(window.AMap);
            }).catch(a);
          }
        else if (o.key && o.key !== e.key)
          a("多个不一致的 key");
        else if (o.version && o.version !== e.AMap.version)
          a("不允许多个版本 JSAPI 混用");
        else {
          var v = [];
          if (o.plugins)
            for (r = 0; r < o.plugins.length; r += 1)
              e.AMap.plugins.indexOf(o.plugins[r]) == -1 && v.push(o.plugins[r]);
          S(function() {
            v.length ? window.AMap.plugin(v, function() {
              p(o).then(function() {
                l(window.AMap);
              }).catch(a);
            }) : p(o).then(function() {
              l(window.AMap);
            }).catch(a);
          });
        }
      });
    }, reset: function() {
      delete window.AMap, delete window.AMapUI, delete window.Loca, e = { key: "", AMap: { version: "1.4.15", plugins: [] }, AMapUI: { version: "1.1", plugins: [] }, Loca: { version: "1.3.2" } }, s = {
        AMap: i.notload,
        AMapUI: i.notload,
        Loca: i.notload
      }, m = { AMap: [], AMapUI: [], Loca: [] };
    } };
  });
})(G);
var fe = G.exports;
const ve = /* @__PURE__ */ pe(fe), me = (t, y) => {
  const p = t.__vccOpts || t;
  for (const [f, I] of y)
    p[f] = I;
  return p;
}, he = V({
  props: {
    width: {
      type: Number,
      default: 700
    },
    height: {
      type: Number,
      default: 500
    },
    config: {
      type: Object,
      required: !0,
      default: {
        securityJsCode: "",
        // 高德安全密匙
        key: "",
        // 高德开发者key
        version: "2.0",
        // api版本
        AMapUIVersion: "1.1"
      }
    }
  },
  name: "VueAmapPicker",
  components: {
    Search: N,
    Location: D,
    ElIcon: ae,
    ElEmpty: se,
    ElInput: re
  },
  emits: ["save"],
  setup(t, { emit: y }) {
    let p = null, { config: f, width: I, height: i } = T(t);
    const e = Y({
      map: null,
      dialogFormVisible: !1,
      title: "",
      keyword: "",
      placeSearch: () => {
      },
      positionPicker: () => {
      },
      autoCompleteSearch: () => {
      },
      // 自动完成搜索
      addressList: [],
      currentItem: {},
      keywordList: [],
      // 关键词列表
      isShowBox: !1,
      scrollDiv: null,
      rowFillForm: {
        lng: "116.397827",
        lat: "39.90374"
      },
      searchOptionedList: [],
      mapStatus: 2
      // 1 禁止新列表数据 2 更新列表数据   地图操作状态
    }), s = (n) => {
      e.isShowBox = !1, e.keywordList = [];
    }, m = async (n) => {
      n.location && (e.keyword = n.name, e.searchOptionedList = [n], l(n, 3));
    };
    function b(n) {
      return new Promise((u, g) => {
        e.autoCompleteSearch.search(n, (w, k) => {
          w == "complete" ? u(k) : u({
            tips: []
          });
        });
      });
    }
    const S = async (n) => {
      let u = await b(n);
      e.keywordList = (u == null ? void 0 : u.tips) || [], e.keywordList.length ? e.isShowBox = !0 : e.isShowBox = !1;
    }, o = (n) => {
      W(S, 1e3)(n);
    }, l = (n, u = 2) => {
      e.mapStatus = u, e.currentItem = n, e.map.setZoom(20, !0), e.positionPicker.start(
        new AMap.LngLat(n.location.lng, n.location.lat)
      );
    }, a = async (n) => {
      p.setContent(`<div class="self-window-wrap">
    <div class="right-section">
    <div class="title">
      <span>${n.name || ""}</span>
    </div>
    <div class="detail-lists">
        <div class="li">
          <div class="dot"></div>
          <span>${n.address || "暂无"}</span>
        </div>
      </div>

      <div class="btn-wrap">
        <div class="confirm" id="confirm_map">确&nbsp;认</div>
      </div>


    </div>

  </div>`), p.open(e.map, e.map.getCenter()), document.getElementById("confirm_map").onclick = () => {
        let u = ["上海市", "北京市", "天津市", "重庆市"];
        n.city, u.indexOf(n.province) > -1 && n.province, y("save", {
          province: n.province,
          city: n.city,
          district: n.district,
          address: n.address,
          name: n.name,
          lat: n.location.lat,
          lng: n.location.lng,
          latLng: n.location.lat + "," + n.location.lng
        });
      };
    }, c = () => {
      window._AMapSecurityConfig = {
        securityJsCode: f.value.securityJsCode
      }, ve.load({
        key: f.value.key,
        version: f.value.version || "2.0",
        AMapUI: {
          version: f.value.AMapUIVersion || "1.1"
        }
      }).then(() => {
        r();
      }).catch((n) => {
        console.error(n, "错误错误");
      });
    }, r = () => {
      e.map = new AMap.Map("container", {
        viewMode: "2D",
        //默认使用 2D 模式
        zoom: 19,
        //地图级别
        center: [116.397428, 39.90923],
        //地图中心点
        scrollWheel: !1,
        zooms: [4, 20]
      }), h(), x(), p = new AMap.InfoWindow({
        isCustom: !0,
        offset: new AMap.Pixel(-0, -38)
      }), p.setMap(e.map), e.map.on("dragstart", (n) => {
        e.mapStatus = 1, e.searchOptionedList = [];
      }), e.map.on("dragend", (n) => {
        e.mapStatus = 2, e.currentItem = {};
      });
    }, h = async () => {
      AMapUI.loadUI(["misc/PositionPicker"], function(n) {
        e.positionPicker = new n({
          mode: "dragMap",
          map: e.map
        });
        let u = "";
        e.rowFillForm.lng && (u = new AMap.LngLat(
          e.rowFillForm.lng,
          e.rowFillForm.lat
        )), u ? e.positionPicker.start(u) : e.positionPicker.start, e.map.panBy(0, 1), v();
      });
    }, v = () => {
      let n = e.positionPicker;
      n.on("success", function(u) {
        let g = u.regeocode.pois || [], w = e.searchOptionedList;
        w.length && (g = g.filter((k) => k.id != w[0].id)), (e.mapStatus == 2 || e.mapStatus == 3) && (e.addressList = [...w, ...g]), _(u);
      }), n.on("fail", function(u) {
        e.addressList = [], e.keyword = "", e.currentItem = {}, p.close();
      });
    }, d = (n, u) => {
      let g = n.findIndex((w) => w.id == u);
      g != 0 && g > -1 && n.unshift(n.splice(g, 1)[0]);
    };
    async function _(n) {
      let u = n.regeocode, g = e.addressList;
      await $(), e.mapStatus == 1 && d(g, e.currentItem.id), U();
      let w = u.addressComponent;
      ["上海市", "北京市", "天津市", "重庆市"].indexOf(w.province) > -1 && (w.city = w.province), a({
        ...w,
        address: n.address,
        location: n.position,
        ...e.currentItem.id ? {
          adcode: e.currentItem.adcode,
          address: e.currentItem.address,
          id: e.currentItem.id,
          location: e.currentItem.location,
          name: e.currentItem.name
        } : {}
      });
    }
    const U = () => {
      e.scrollDiv.scrollTo({ top: 0, behavior: "smooth" });
    }, x = () => {
      AMap.plugin(
        ["AMap.PlaceSearch", "AMap.AutoComplete", "AMap.ToolBar"],
        function() {
          e.autoCompleteSearch = new AMap.AutoComplete({}), e.placeSearch = new AMap.PlaceSearch({
            pageSize: 20,
            pageIndex: 1,
            extensions: "all",
            type: "",
            citylimit: !1
            //是否强制限制在设置的城市内搜索
          });
          let n = new AMap.ToolBar();
          e.map.addControl(n);
        }
      );
    }, H = () => {
      e.dialogFormVisible = !1, e.mapStatus = 2, e.rowFillForm = {
        lng: "116.397827",
        lat: "39.90374"
      }, e.addressList = [], e.searchOptionedList = [], e.currentItem = {}, e.keyword = "";
    }, j = () => {
      e.title = "地图定位", e.dialogFormVisible = !0, e.rowFillForm = {
        lng: "116.397827",
        lat: "39.90374"
      }, $(() => {
        c();
      });
    }, Z = (n) => {
      if (Object.is(n)) {
        let { securityJsCode: u, key: g, version: w, AMapUIVersion: k, width: K, height: Q } = n;
        if (!g || !u) {
          console.error("请填写key或高德安全密匙");
          return;
        }
        let X = {
          securityJsCode: u,
          key: g,
          version: w || f.value.version,
          AMapUIVersion: k || f.value.AMapUIVersion,
          width: K || f.value.width,
          height: Q || f.value.height
        };
        f.value = X;
      } else
        console.error("参数格式错误");
    };
    return R(() => {
      j(), console.log("加载完成");
    }), ee(() => {
      e.map.destroy(), console.log("卸载卸载");
    }), {
      moveLatLng: (n = {}) => {
        if (!n.lat || !n.lng) {
          le({
            message: "经纬度不能为空",
            type: "warning"
          });
          return;
        }
        l({ location: n });
      },
      ...T(e),
      config: f,
      close: H,
      Search: N,
      onSearch: m,
      goLocation: l,
      debounce: W,
      onSearchFn: o,
      Location: D,
      onBlur: s,
      showEdit: j,
      loader: Z,
      width: I,
      height: i
    };
  }
}), we = /* @__PURE__ */ A("div", {
  id: "container",
  class: "container"
}, null, -1), ge = { class: "right-contaner-search" }, Me = { class: "input-wrapp" }, Ae = {
  key: 0,
  class: "susegest-wrap"
}, ye = ["onMousedown"], Ie = { class: "na" }, _e = { style: { color: "#999", "font-size": "12px" } }, Le = /* @__PURE__ */ A("div", { class: "tips" }, "请在下面的列表选择一个位置", -1), be = {
  class: "ul infinite-list",
  ref: "scrollDiv"
}, Ue = ["onClick"], ke = { class: "name" }, Se = { class: "address" };
function Ce(t, y, p, f, I, i) {
  const e = C("el-input"), s = C("Search"), m = C("el-icon"), b = C("Location"), S = C("el-empty");
  return M(), L("div", {
    class: "role-management-container-liyingqi",
    style: oe({ width: t.width + "px", height: t.height + "px" })
  }, [
    we,
    A("div", ge, [
      A("div", Me, [
        F(e, {
          modelValue: t.keyword,
          "onUpdate:modelValue": y[0] || (y[0] = (o) => t.keyword = o),
          placeholder: "搜索地点",
          "prefix-icon": t.Search,
          class: "input",
          onInput: t.onSearchFn,
          onBlur: t.onBlur
        }, null, 8, ["modelValue", "prefix-icon", "onInput", "onBlur"]),
        t.isShowBox ? (M(), L("div", Ae, [
          (M(!0), L(B, null, J(t.keywordList, (o, l) => (M(), L(B, null, [
            o.location ? (M(), L("div", {
              class: "li",
              key: l,
              onMousedown: ne((a) => t.onSearch(o), ["stop"])
            }, [
              o.location ? (M(), O(m, { key: 1 }, {
                default: z(() => [
                  F(b)
                ]),
                _: 1
              })) : (M(), O(m, { key: 0 }, {
                default: z(() => [
                  F(s)
                ]),
                _: 1
              })),
              A("div", Ie, [
                te(P(o.name) + " ", 1),
                A("span", _e, P(o.district ? `(${o.district})` : ""), 1)
              ])
            ], 40, ye)) : E("", !0)
          ], 64))), 256))
        ])) : E("", !0)
      ]),
      Le,
      A("div", be, [
        (M(!0), L(B, null, J(t.addressList, (o) => (M(), L("div", {
          class: ie({ li: !0, active: o.id == t.currentItem.id }),
          key: o.id,
          onClick: (l) => t.goLocation(o, 1)
        }, [
          A("div", ke, P(o.name), 1),
          A("div", Se, P(o.address || "暂无详细地址"), 1)
        ], 10, Ue))), 128)),
        t.addressList.length ? E("", !0) : (M(), O(S, {
          key: 0,
          description: "暂无地点数据"
        }))
      ], 512)
    ])
  ], 4);
}
const q = /* @__PURE__ */ me(he, [["render", Ce]]), Be = {
  install(t) {
    console.log(t), t.component(q.name, q);
  }
};
export {
  Be as default
};
