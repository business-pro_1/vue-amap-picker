# vue-amap-picker

`vue-amap-picker` 是一款适用于 `vue3` 的高德地图选点插件。

`vue-amap-picker` 封装了地图拖动选点，关键词联想搜索，用自定义信息窗体的形式展示选择拖动的和搜索的经纬度所对应的地址详细信息。

## 安装

### npm

```
npm install -S vue-amap-picker
```

### yarn

```bash
yarn add vue-amap-picker
```

## 使用

### 引入

在 vue 实例化 `main.js` 中使用它

```javascript
import { createApp } from 'vue';
import App from './App.vue';
import AmapPicker from 'vue-amap-picker';
import 'vue-amap-picker/dist/style.css'; //引入css
const app = createApp(App);
app.use(AmapPicker);
app.mount('#app');
```

### 项目截图

<div style="text-align:center;">
  <img src="./image/order.png" width="960px" height="auto"/>
</div>

### 引入

页面中使用

```javascript
<script setup>
  import { ref } from 'vue';
  let config = ref({
    securityJsCode: '', // 高德安全密匙 必填
    key: '', // 高德开发者key 必填
    version: '2.0', // api版本 缺省2.0
    AMapUIVersion: '1.1',// AmapUi版本 缺省 1.1
  });
  let map = ref(null);

  setTimeout(() => {
    // 地图点移动组件实例接口
    map.value.moveLatLng({
      lat: '30',
      lng: '119',
    });
  }, 4000);

  let save = (options)=> { //回调参数
  //  options参数说明
    options = {
      province: '黑龙江省',
      city: '哈尔滨市',
      district: '松北区',
      address: '松北一路',
      name: '监管局', // 可能为空
      lat: '116.397827',
      lng: '39.90374',
      latLng:'39.90374,116.397827'
    }
  }
</script>
<template>
  <VueAmapPicker
    ref="map"
    :config="config"
    :height="500"
    :width="700"
    @save="save"
  ></VueAmapPicker>
</template>
<style lang="scss" scoped></style>
```

### 注意

组件最小宽度 700 最大高度 500 不填默认值 就是 700 和 500

### 请作者喝杯咖啡

<div style="text-align:center;">
  <img src="./image/pay.jpg" width="300px" height="300px"/>
</div>
